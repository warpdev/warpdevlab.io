# Cisco

> [Cisco Systems, Inc.](https://.cisco.com/) is an American multinational technology conglomerate headquartered in San Jose, California, in the center of Silicon Valley, that develops, manufactures and sells networking hardware, telecommunications equipment and other high-technology services and products. Through its numerous acquired subsidiaries, such as OpenDNS, WebEx, Jabber and Jasper, Cisco specializes into specific tech markets, such as Internet of Things (IoT), domain security and energy management. Cisco is the largest networking company in the world.

> source: [Wikipedia](https://en.wikipedia.org/wiki/Cisco_Systems)

For developers Cisco created [DevNet](https://developer.cisco.com/): 

> It’s the place to create and find inspiring applications, learn about our APIs, and connect with other developers in our communities.

## Cisco and Warpcom

[Dimension Data](https://www.dimensiondata.com) is Cisco's first Global Gold partner, and these companies have 25 years of experience delivering solutions together.
Warpcom was Dimension Data until 2016, which made us one of the most important Cisco system integrators in Portugal.

Check [our history](https://warpcom.com/en/warpcom-history/) to understand how we've grown since then while keeping a close relation to Cisco and Dimension Data.

Warpcom is currently a Cisco Gold Partner with advanced architecture specializations in Security, Collaboration, Data Center and Enterprise Networks.

## DevNet

APIs, SDKs, Sandbox, and Community for Cisco Developers, organized around four pillars:

* [Learn](#learn)
* [Code](#code)
* [Inspire](#inspire)
* [Connect](#connect)

![Cisco DevNet](../static/img/cisco/devnet.png "Cisco DevNet")

### Learn

#### [Learning Tracks](https://learninglabs.cisco.com/tracks) 

Provide a guided path to learn selected Cisco technologies. 
Based on [DevNet Learning Labs(https://learninglabs.cisco.com/), a track is made up of modules, which in turn are composed of learning labs. Some modules also focus on general knowledge in programming, networking, or other topics.

#### [Video course](https://developer.cisco.com/video/net-prog-basics/)

Network programmability expert-led video course. With six modules, each with lessons including API and code samples that can be used on any computer to follow along with the videos.

### Code

#### [Sandbox Remote Labs](https://developer.cisco.com/site/devnet/sandbox/)

A virtual environment with hosted, live infrastructure 
Provides instant access to platforms to help quickly and easily explore, create and test solutions.

It can be used instantly in a shared environment with limited functionality (Always-on) or in a private environment with full functionality (Reservation). Both are self-service.

#### [Sample Code on Github](http://ciscodevnet.github.io)

Sample code, developed by Cisco on Github, to help the understanding of Cisco APIs.

### Inspire

#### [DevNet Creations](https://creations.devnetcloud.com/)

A showcase of innovation projects built with Cisco’s technology.


### Connect 

* Join [communities](https://developer.cisco.com/site/coi/) of interest and participate in on-going discussions around technologies.

* Meet and greet at [events](https://developer.cisco.com/site/devnet/events-contests/events/) around the world

