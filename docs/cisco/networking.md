# The Network as a Platform

This page discusses Cisco's networking solutions enabled by WarpDev.


## Cisco Connected Mobile Experiences (CMX)

> Cisco Connected Mobile Experiences (CMX) is a smart Wi-Fi solution that uses the Cisco wireless infrastructure to detect and locate consumers’ mobile devices. With it, your organization can directly deliver content to smartphones and tablets that’s personalized to your visitors’ preferences and pertinent to their real-time indoor locations.
>
> source: [developer.cisco.com](https://developer.cisco.com/site/cmx-mobility-services/)

### CMX Engage <img alt="Deployed in WrpLab" src="../../static/img/wrplab.png" title="Solution deployed in Warpcom Lab" style="width: 25px">

> Cisco CMX Engage delivers customer engagements that are dynamic, personalized and relevant. Get location insights and engaging portal, SMS, and app experiences.
>
> source: [cisco.com](https://www.cisco.com/c/en/us/solutions/enterprise-networks/connected-mobile-experiences/cmx-engage.html)

Deployed as a SaaS solution, CMX Engage is compatible with [Meraki](https://meraki.cisco.com/), CMX, and [WLC](https://www.cisco.com/c/en/us/products/wireless/wireless-lan-controller/index.html).

#### Features 

- **SDK for Android and iOS** that provide easy access to WiFi location APIs and Subscriber APIs.
- **Subscriber APIs** where it is possible to set [captive portal](https://www.cisco.com/c/en/us/td/docs/solutions/Enterprise/Mobility/CMX-Engage/cmxengage-configuration-guide/cmxengage-cuwn/cmxengage-CUWN/captive-portals.html#pgfId-1333254)  (registration) and [engagement](https://www.cisco.com/c/en/us/td/docs/solutions/Enterprise/Mobility/CMX-Engage/cmxengage-configuration-guide/cmxengage-cuwn/cmxengage-CUWN/Proximity-Engine.html#pgfId-1304380) (entering, exiting, staying for several minutes or at a particular time in one or several zones) rules that do GET or POST requests to an API.
- **[Streaming Data Export API](https://www.cisco.com/c/en/us/td/docs/solutions/Enterprise/Mobility/CMX-Engage/API-docs/CMX-Engage-data-export.html)** that allows the automatic export, in CSV format, of data like User Acquisition, User Visits, and Engagements. This API is optimized to support easy ingestion into CRM / Big Data / Analytics / BI systems and can be customized to provide non-standard data.

#### Example 

Providing an example that covers all the features, you may have the following actions that can be triggered when a customer connects:

- An App Push notification saying "Welcome!" is sent to the user device, using the SDK capabilities;
- An external API POST request, formatted in JSON, is made to the API of some system (CRM or Marketing platform,  for example) utilizing the Subscriber APIs;

The filters for triggering these notifications are Location (Where do you want the rule to fire?), Identity (Who do you want the rule to apply?), Schedule and Action. 

Additionaly, a list of Users that have entered that network can be sent to a system (CRM, Big Data, Analytics or BI, for example) by the Streaming Data Export API, with the frequency of the upload customized to suit specific requirements and volume of data.


#### Learning & Docs

Demo environments can be set on demand! 
In the demo console, you have access to the SDKs, API Docs and so on.


### CMX On-Premise <img alt="Deployed in WrpLab" src="../../static/img/wrplab.png" title="Solution deployed in Warpcom Lab" style="width: 25px"> <img alt="Used in Warpcom" src="../../static/img/warpcom.png" title="Solution used in Warpcom" style="width: 25px">

> Use the intelligence in your wireless network. With CMX, view location information, dwell times, and analytics to learn how visitors behave on the site. Our on-premises platform gives you complete control and ownership, and is at the heart of several CMX solutions.
>
> source: [cisco.com](https://www.cisco.com/c/en/us/solutions/enterprise-networks/connected-mobile-experiences/cmx-on-premise.html)

#### Features 

CMX offers the following **APIs**:

- **Configuration** - Configure different aspects of MSE.
- **Location** - Find location specific details on visitors.
- **Analytics** - Find analytical data on visitors.
- **Connect** - Find user session information.
- **Presence** - Find presence data on visitors.

Particular note for the Configuration REST APIs where it is possible to subscribe to notifications such as user location change, association and so on.

#### Example 

As an example, we can look at integrating with a CRM system when a user connects to the Wi-Fi.
The user arrives and through the captive portal provides its name, e-mail or other information (phone number (verified or not), a specific question) to gain access to the network.

1. Using the Configuration API set a notification to be informed of new associations.
2. Receive a JSON POST notification when a new user joins the Wi-Fi.  
3. Get the information provided by the user using the Connect API.
4. Add the user information to the CRM system, or complement the user profile with this new visit,  using its API.


#### Learning

Cisco provides the [Using the Cisco Location Based Services](https://learninglabs.cisco.com/modules/dna-cmx-mse) module on DevNet.

#### API Docs

The documentation on the API can be found in [cmxlocationsandbox.cisco.com/apidocs](https://cmxlocationsandbox.cisco.com/apidocs/).

To use the live API test use the following credentials:

- username: ``learning``
- password: ``learning``

